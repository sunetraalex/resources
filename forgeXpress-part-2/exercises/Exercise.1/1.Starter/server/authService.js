const express = require('express');
const forge = require('forge-apis');

class AuthService {
    constructor(options) {
        this._options = options;
        this._router = express.Router();
        this.initializeRoutes();
    }

    get router() {
        return this._router;
    }

    initializeRoutes() {
        this.router.post('/viewtoken', (req, res) => {
            this.createViewToken(req, res);
        });
    }

    async createViewToken(req, res) {
        // TODO: Step 1 - implement createViewToken to create and return 2L authentication token
    }
}

exports.AuthService = AuthService;