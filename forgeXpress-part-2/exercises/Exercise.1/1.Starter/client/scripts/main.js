
//Replace placeholder below by your encoded model urn
const urn = '<YOUR_URN>';

let viewerApp;
const documentId = `urn:${urn}`;

/**
 * Wait until document is ready and then initialize viewer and display given document.
 */
$(document).ready(() => {
    console.log('Document is ready');
    // TODO: Step 3 - extend callback to initialize viewer
});

/**
 * Obtains authentication token. The information is returned back via callback.
 * @param {callback} callback - callback to pass access token and expiration time.
 */
function getToken(callback) {
    $.ajax({
        url: '/api/auth/viewtoken',
        type: 'POST',
        dataType: 'json',
        success: (response) => {
            callback(response.access_token, response.expires_in);
        },
        error: (response) => {
            console.error(response.statusText);
        }
    });
}

/**
 * Autodesk.Viewing.Document.load() success callback.
 * Proceeds with model initialization.
 * @param {Autodesk.Viewing.Document} doc - loaded document
 */
function onDocumentLoadSuccess(doc, errorsAndWarnings) {
    // TODO: Step 4 - find and select viewable
}

/**
 * Autodesk.Viewing.Document.load() failure callback.
 * @param {number} errorCode 
 */
// TODO: Step 5 - add function to implement error handler

/**
 * loadDocumentNode resolve handler.
 * Invoked after the model's SVF has been initially loaded.

 * @param {Object} item 
 */
// TODO: Step 5 - add function to implement item load success handler

/**
 * loadDocumentNode reject handler.
 * @param {Object} error 
 */
// TODO: Step 5 - add function to implement item load error handler
