class SamplePanel extends Autodesk.Viewing.UI.DockingPanel {
    constructor(viewer, id, options) {
        super(viewer.container, id, 'Sample', options);
        this.viewer = viewer;
        this.container.classList.add('sample-panel');
        this.container.style.left = '60px';
        this.container.style.top = '40px';
        this.container.style.width = '340px';
        this.container.style.height = '190px';
        this.container.style.position = 'absolute';
        // scroll container
        this.createScrollContainer({
            heightAdjustment: 70,
            left: false,
            marginTop: 0
        });
        // create UI
        const url = `${window.location.href}scripts/extensions/res/samplePanel.html`;

        Autodesk.Viewing.Private.getHtmlTemplate(url, (err, content) => {
            this.onTemplate(err, content);
        });
    }

    toggleVisibility() {
        this.setVisible(!this.isVisible());
    }

    onApply() {
        this.viewer.clearThemingColors(this.viewer.model);
        const propertyName = this._propertyName.val();
        const propertyValue = this._propertyValue.val();

        if ((propertyName.length === 0) || (propertyValue.length === 0)) {
            return;
        }
        this.viewer.model.getObjectTree((instanceTree) => {
            const ids = [];

            instanceTree.enumNodeChildren(instanceTree.getRootId(), (id) => {
                if (instanceTree.getChildCount(id) === 0) {
                    ids.push(id);
                }
            }, true);
            const properties = [];

            properties.push(propertyName);
            this.viewer.model.getBulkProperties(ids, properties, (propResults) => {
                const matchingIds = [];

                propResults.forEach((propResult) => {
                    propResult.properties.forEach((property) => {
                        if ((property.displayName === propertyName) && (property.displayValue === propertyValue)) {
                            matchingIds.push(propResult.dbId);
                        }
                    });
                });
                // Color vector (R, G, B, ALPHA)
                const color = new THREE.Vector4(1, 0, 0, 0.5);

                matchingIds.forEach((id) => {
                    this.viewer.setThemingColor(id, color, this.viewer.model, true);
                });
                this.viewer.isolate(matchingIds);
            });
        });
    }

    onClose() {
        this.viewer.clearThemingColors(this.viewer.model);
        this.viewer.isolate();
        this.setVisible(false);
    }

    onTemplate(err, content) {
        const tmp = document.createElement('div');

        tmp.innerHTML = content;
        this.scrollContainer.appendChild(tmp.childNodes[0]);
        // bind controls
        this._propertyName = $('#property-name');
        this._propertyValue = $('#property-value');
        this._applyBtn = $('#apply-btn');
        this._applyBtn.on('click', () => {
            this.onApply();
        });
        this._closeBtn = $('#close-btn');
        this._closeBtn.on('click', () => {
            this.onClose();
        });
    }
}
