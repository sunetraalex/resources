class SamplePanel extends Autodesk.Viewing.UI.DockingPanel {
    constructor(parentContainer, id, options) {
        super(parentContainer, id, 'Sample', options);
        this.container.classList.add('sample-panel');
        this.container.style.left = '60px';
        this.container.style.top = '40px';
        this.container.style.width = '200px';
        this.container.style.height = '160px';
        this.container.style.position = 'absolute';
        // scroll container
        this.createScrollContainer({
            heightAdjustment: 70,
            left: false,
            marginTop: 0
        });
        // TODO: Step 2 - load panel

}

    toggleVisibility() {
        this.setVisible(!this.isVisible());
    }

    onTemplate(err, content) {
        // TODO: Step 3 - add code to inject HTML to the DOM    
    }
}

