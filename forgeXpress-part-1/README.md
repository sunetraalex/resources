# Link utili

## Materiale
* [Slide](https://cutt.ly/forgeXpress-day1-2)
* [Files](https://cutt.ly/forge-files)
* [Recording](https://app.box.com/s/t9eqrnmuwtgdje0prronlmnnbiglpoe1)

## Documentazione
* [Forge](https://forge.autodesk.com/developer/documentation)
* [Stack overflow](https://stackoverflow.com/questions/tagged/autodesk-forge)
* [Bucket Retention Policy](https://cutt.ly/forge-retention)
* [Forge Scopes](https://cutt.ly/forge-scopes)
* [REST](restfulapi.net)
* [JSON](json.org)
* [JavaScript](javascript.com)

## Strumenti
* [Zapier] account(https://zapier.com/)
* [Postman](https://www.postman.com/downloads/)
* [Newman](https://learning.postman.com/docs/postman/collection-runs/command-line-integration-with-newman/) - Command line integration per Postman
* [JSFiddle](jsfiddle.net) - IDE Online

## Approfondimenti
* [Google](google.com)

