# ForgeXpress

This project contains resources for ForgeXpress, an introduction to Autodesk Forge.

## Contents Overview

### Part 1
* Collections of Postman tasks to explore Autodesk Forge APIs (Authentication, Data Management, Model Derivatives, Webhooks)
* Environment variables definitions for Postman
* Useful links
* 3d model to play with
### Part 2
* Collection of Postman tasks to prepare the work environment
* Environment variables definitions for Postman
* Exercises to familiarize with Autodesk Forge Viewer API (courtesy of Forge Starter Package Service Offering (PSO))
* Useful links
* 3d model to play with

## Prerequisites
### Overall
* Visit [Forge Developer Portal](https://developer.autodesk.com), sign up for account.
* Create new app.
* Take note of **Client ID** and **Client Secret**.
* Install [Postman](https://www.postman.com/downloads/)
### Part 1
* Sign up for a [Zapier](https://zapier.com/) account
### Part 2
* Install [Node.JS](https://nodejs.org).
* Install [VSCode](https://code.visualstudio.com/Download).
* Install [Git](https://git-scm.com/downloads).
* For each exercise run `npm install` from the **Starter** folder.
  
  **Note** Initial version of an exercise is available in **Starter** folder. If you prefer to review final version, use **Finished** folder.

## License
